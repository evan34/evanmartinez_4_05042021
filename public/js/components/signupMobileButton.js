
/**
 * Create signup button for tablet and mobile devices
 */
export const signupMobileButton = function () {
  const heroSection = document.querySelector('.hero-section');
  const button = document.createElement('button');
  const textButton = document.createTextNode('je m\'inscris');
  button.setAttribute('type', 'button');
  button.classList.add('btn-signup');
  button.classList.add('modal-btn');
  button.appendChild(textButton);

  heroSection.appendChild(button);
}