/**
  * Create modal success element
  */
export const openSuccessModal = () => {

  const bground = document.createElement('div');
  bground.classList.add('bground');
  bground.classList.add('success-bground');
  const content = document.createElement('div');
  content.classList.add('content');
  const span = document.createElement('span');
  span.classList.add('close-success');
  span.classList.add('close');
  const modalBody = document.createElement('div');
  modalBody.classList.add('modal-body');
  const div = document.createElement('div');
  div.classList.add('success-modal');
  const paragraph = document.createElement('p');
  const text = document.createTextNode("Merci d'avoir soumis vos informations d'inscription !");

  bground.appendChild(content);
  content.appendChild(span);
  content.appendChild(modalBody);
  modalBody.appendChild(div);
  div.appendChild(paragraph).appendChild(text);

  const closeButton = document.createElement('button');
  closeButton.classList.add('btn-submit');
  closeButton.classList.add('btn-close');

  const textButton = document.createTextNode('Fermer');
  closeButton.appendChild(textButton);
  modalBody.appendChild(closeButton);

  bground.style.display = 'block';

  const main = document.querySelector('main');
  main.appendChild(bground);
}