/**
 *  Inputs handler
 *  Send error or success message to client interface
 */
export const validateInputs = () => {

  const regDate = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
  const regNumber = /^[0-9]\d*$/;
  const regEmail = /^[a-zA-Z0-9.-_]+[@]{1}[a-zA-Z0-9._]+[.]{1}[a-z]{2,10}$/;
  const locationEls = document.querySelectorAll('input[name="location"]');
  const checkbox1El = document.querySelector('input[name="checkbox1"]');
  let formInputs = document.querySelectorAll('input');


  formInputs.forEach(inputElement => {
    let inputNameValue = inputElement.name;
    let value = inputElement.value;

    switch (inputNameValue) {
      case 'firstname':
        if (value.trim().length < 2 || !/^[a-zéèàçA-Z\-]+$/.test(value)) {
          setErrorMessage(inputElement, "Saisissez au moins deux lettres !");
        } else {
          setSuccessMessage(inputElement);
        }
        break;
      case 'lastname':
        if (value.trim().length < 2 || !/^[a-zA-Z]+$/.test(value)) {
          setErrorMessage(inputElement, "Saisissez au moins deux lettres !");
        } else {
          setSuccessMessage(inputElement);
        }
        break;
      case 'email':
        if (!regEmail.test(value)) {
          setErrorMessage(inputElement, "Saisissez un email valide !");
        } else {
          setSuccessMessage(inputElement);
        }
        break;
      case 'birthdate':
        console.log(value);
        if (!regDate.test(value)) {
          setErrorMessage(inputElement, "Veuillez saisir une date !");
        } else {
          setSuccessMessage(inputElement);
        }
        break;
      case 'quantity':
        if (regNumber.test(value)) {
          setSuccessMessage(inputElement);
        } else {
          setErrorMessage(inputElement, "Vous devez saisir un nombre !");
        }
        break;
      case 'location':
        let checkboxChecked = 0;
        for (let i = 0; i < locationEls.length; i++) {
          if (locationEls[i].checked) {
            checkboxChecked++;
          }
        }
        if (checkboxChecked > 0) {
          setSuccessMessage(inputElement);
        } else {
          inputElement = document.querySelector('input[id="location1"]')
          setErrorMessage(inputElement, "Vous devez sélectionner une ville !");
        }
        break;
      case 'checkbox1':
        if (!checkbox1El.checked) {
          setErrorMessage(checkbox1El, "Veuillez accepter les CGU !");
        } else {
          setSuccessMessage(checkbox1El);
        }
        break;
    }
  })
}


/**
 * @param {HTMLElement} input 
 * @param {String} message 
 */
function setErrorMessage(input, message) {
  const formControl = input.parentElement;
  formControl.setAttribute('data-success-visible', 'false');
  formControl.setAttribute('data-error-visible', 'true');
  formControl.setAttribute('data-error', message);
}

/**
 * Success handler message 
 * @param {HTMLElement} input 
 */
function setSuccessMessage(input) {
  const formControl = input.parentElement;
  formControl.removeAttribute('data-error-visible');
  formControl.removeAttribute('data-error');
  formControl.setAttribute('data-success-visible', 'true');
}