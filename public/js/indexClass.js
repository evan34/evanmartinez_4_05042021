import { validateInputs } from './validateForm.js';
import { openSuccessModal } from './components/successModal.js';
import { signupMobileButton } from './components/signupMobileButton.js';

/**
 *  Manage form features
 *  @property {HTMLElement} body
 *  @property {HTMLElement} modalEl
 *  @property {HTMLElement} closeModalButton
 *  @property {HTMLElement} formsData
 *  @property {HTMLElement} main 
 *  @property {HTMLElement} formEls
 */
class Form {
  constructor(form) {
    this.form = form;
    this.body = document.querySelector('body');
    this.modalEl = document.querySelector('.bground');
    this.closeModalButton = document.querySelector('.close');
    this.formsData = document.querySelectorAll('.formData');
    this.main = document.querySelector('main');
    this.formEls = document.querySelectorAll('.formData');
  }

  /**
   *  Add event listener to both signup buttons
   *  One for desktop device and the other for mobile
   *  and tablett devices.
   */
  onClickSignup = function () {
    signupMobileButton();
    this.openModalButtons = document.querySelectorAll('.modal-btn')
      .forEach(button => {
        button.addEventListener('click', this.openModal);
      });
  }

  openModal = () => {
    this.form.style.display = 'block';
    this.modalEl.style.display = 'block';
    this.body.style.overflow = 'hidden';
    this.closeModalButton.addEventListener('click', this.closeModal);
    this.submitForm();
  }

  closeModal = () => {
    this.formsData.forEach(el => {
      el.setAttribute('data-success-visible', 'false');
      el.setAttribute('data-error-visible', 'false');
      el.querySelector('input').value = '';
    });
    this.body.style.overflow = 'visible';
    this.modalEl.style.display = 'none';
  }

  closeSuccessModal = () => {
    this.succesModal = document.querySelector('.success-bground');
    this.main.removeChild(this.succesModal);
    this.closeModal();
  }

  /**
   *  Check if all the inputs have
   *  validate values
   */
  checkValidInputs = () => {
    let successInputs = 0;
    this.formEls.forEach(el => {
      if (el.dataset.successVisible === "true") {
        successInputs++;
      }
    })
    return successInputs;
  }

  submitForm = () => {
    this.form.addEventListener('submit', e => {
      e.preventDefault();
      validateInputs();

      // Check if the number of valid inputs equals the number of inputs
      if (this.checkValidInputs() === this.formEls.length) {
        this.form.style.display = 'none';
        this.form.reset();
        this.closeModal();
        openSuccessModal();

        this.body.style.overflow = 'hidden';
        this.successModalButton = document.querySelector('.btn-close');
        this.closeSpanButton = document.querySelector('.close-success');
        this.successModalButton.addEventListener('click', this.closeSuccessModal);
        this.closeSpanButton.addEventListener('click', this.closeSuccessModal);
      }
    })
  }
}

const form = document.querySelector('#form');

new Form(form).onClickSignup();

/**
 * Show current year to footer
 */
function date() {
  let date = new Date().getFullYear();
  document.querySelector('.copyrights').innerHTML = `Copyright 2014 - ${date}, GameOn Inc.`
}

date();

/**
 * Show burger menu on mobile device
 */
const mainNavbar = document.querySelector('.main-navbar');
mainNavbar.addEventListener('click', function () {
  let x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
})